$LOGFILE = "c:\logs\plugin-windows-vscode.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

function Install-VSCode {
  [CmdletBinding()]
  param(
      [parameter()]
      [ValidateSet("64-bit", "32-bit")]
      [string]$Architecture = "64-bit",

      [parameter()]
      [ValidateSet("stable", "insider")]
      [string]$BuildEdition = "stable",

      [Parameter()]
      [ValidateNotNull()]
      [string[]]$AdditionalExtensions = @(),

      [switch]$LaunchWhenDone
  )

  if (($PSVersionTable.PSVersion.Major -le 5) -or $IsWindows) {
      switch ($Architecture) {
          "64-bit" {
              if ((Get-CimInstance -ClassName Win32_OperatingSystem).OSArchitecture -eq "64-bit") {
                  $codePath = $env:ProgramFiles
                  $bitVersion = "win32-x64"
              }
              else {
                  $codePath = $env:ProgramFiles
                  $bitVersion = "win32"
                  $Architecture = "32-bit"
              }
              break;
          }
          "32-bit" {
              if ((Get-CimInstance -ClassName Win32_OperatingSystem).OSArchitecture -eq "32-bit"){
                  $codePath = $env:ProgramFiles
                  $bitVersion = "win32"
              }
              else {
                  $codePath = ${env:ProgramFiles(x86)}
                  $bitVersion = "win32"
              }
              break;
          }
      }
      switch ($BuildEdition) {
          "stable" {
              $codeCmdPath = "$codePath\Microsoft VS Code\bin\code.cmd"
              $appName = "Visual Studio Code ($($Architecture))"
              break;
          }
          "insider" {
              $codeCmdPath = "$codePath\Microsoft VS Code Insiders\bin\code-insiders.cmd"
              $appName = "Visual Studio Code - Insiders Edition ($($Architecture))"
              break;
          }
      }
      try {
          $ProgressPreference = 'SilentlyContinue'

          if (!(Test-Path $codeCmdPath)) {
              Write-Log "Downloading latest $appName..."
              Remove-Item -Force "$env:TEMP\vscode-$($BuildEdition).exe" -ErrorAction SilentlyContinue
              Invoke-WebRequest -Uri "https://code.visualstudio.com/sha/download?build=$($BuildEdition)&os=$($bitVersion)" -OutFile "$env:TEMP\vscode-$($BuildEdition).exe"

              Write-Log "Installing $appName..."
              Start-Process -Wait "$env:TEMP\vscode-$($BuildEdition).exe" -ArgumentList /silent, /mergetasks=!runcode
          }
          else {
              Write-Log "$appName is already installed."
          }

          $extensions = @("ms-vscode.PowerShell") + $AdditionalExtensions
          foreach ($extension in $extensions) {
              Write-Log "Installing extension $extension..."
              & $codeCmdPath --install-extension $extension
          }

          if ($LaunchWhenDone) {
              Write-Log "Installation complete, starting $appName..."
              & $codeCmdPath
          }
          else {
             Write-Log "Installation complete!"
          }
      }
      finally {
          $ProgressPreference = 'Continue'
      }
  }
  else {
      Write-Log "This script is currently only supported on the Windows operating system."
  }
}

Function Main {

  Write-Log "Start plugin-windows-vscode"
 
  try {
    Install-VSCode -Architecture "64-bit" -BuildEdition "stable" -AdditionalExtensions 'ms-python.python', 'ms-toolsai.jupyter', 'ms-vscode.powershell'
  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  
  Write-Log "End plugin-windows-vscode"
 
}

Main    

